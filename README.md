# Intership-Angular-Bao
1. Angular is a framework to build client side application using HTML, CSS, TypeScript. 
It's great for Single Page Application where parts of the view get refresh asynchronously without have to reload entire the page.

- Modular approach: your application will have a clear structure
- Re-usable code: Angular using component to reuse code
- Development quicker and easier: Angular has a lot of inbuilt capabilities such and validation and routing. Posible to use unit testable


2. AngularJS released in 2010 and Angular 2.0 released in 2016, so Angular's version from 2.0x has removed the word "JS".
AngularJS use Controller and ES5, ES6 and support Dart. For Angular use Component and add suport for TypeSCript.

3. ES6 is considered a set of advanced techniques of Javascript and is the latest version.
Some features of ES6:

- Block - Scoped Constructs Let and Cont: using let to initialize a variable in a block scope
	
- Arrow Function: New function definition by using =>
var hello => name => console.log("Hello " + name);

- Destructuring Assignment: Divide elements of Array or Object into multiple variables with a single code.
let date = ["05", "02", "2019"];
let[d, m, y] = date;// d = "05", m = "02", y = "2019"


- Default Parameters: The default value of the parameter when passed to the function
var hello => (domain = 'hello') => console.log(domain)

- Rest Parameter: Declare a function with an unknown number of parameters
var hellp -> (firstName, lastName, ...other) => {
	return "My name is " + firstName + " " + lastName + " and my information is " + other; 
}

- Template Literals in ES6: How to display variables in the string

var hellp -> (firstName, lastName, ...other) => {
	return `My name is $firstName $lastName and my information is $other`; 
}


- Multi-line String: Write a line of code into multiple lines
`Hello my name is Bao
I'm practicing in the company JVB Vietnam`

- Enhanced Object Literals: Describe the object in a more clear way

+ Property value shorthand: assigns a value to the property when the named property matches the name of the variable
function insertUser (name, age) {
  return {
   name,
   age,
  }
}

+ Computed property key: Set the name of the property more flexibly
let username = "bao";
let salary = 1000;
function createSinger (username: string, salary: salary) {
    return {
      ['salaryOf' + username] : salary,
    }
  }
console.log(createSinger(username, salary)); //{salaryOfBao: 1000}

- Promises: Handling the outcome of a specific action, the outcome of each action will be successful or failed
var promise = new Promise(function(resolve, reject){
    reject('Error!');
    resolve('Success!');
});
 
 
promise.then(
    function(success){
        console.log(success);
    },
    function(error){
        console.log(error);
    }
);



- Classes in ES6: an class similar Object-oriented programming

class Employee {
    public employeeName: string,

    constructor(name: string) {
        this.employeeName = name;
    }

    greet() {
        console.log(`Welcome home ${this.employeeName}`);
    }
}



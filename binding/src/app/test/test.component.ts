import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test',
  template:
      `<h2>Welcome John {{name}}</h2>
        <input #myInput type="text">
  <button (click)="logMessage(myInput)">Log</button>
<!--        <button (click)="onClick($event)">Greet</button>-->
<!--        <h2>{{greeting}}</h2>-->
<!--  <h2 [style.color]="hasError ? 'red': 'green'">John Doe</h2>-->
  
<!--  <h2 [style.color]="highlightColor">John Doe</h2>-->
<!--  <h2 [ngStyle]="titleStyles">John Doe</h2>-->
  <!--  <h2 [class]="classSuccess">Success!!!</h2>-->
  <!--  <h2 [class.text-danger]="hasError">Danger!!!</h2>-->
  <!--  <h2 [ngClass]="messageClasses">Message</h2>-->
  <!--  <h2>{{2 + 2}}</h2>-->
  <!--  <h2>{{"Welcome " + name}}</h2>-->
  <!--  <h2>{{name.length}}</h2>-->
  <!--  <h2>{{name.toUpperCase()}}</h2>-->
  <!--  <h2>{{greetUser()}}</h2>-->
  <!--  <h2>{{siteUrl}}</h2>-->
  <!--  <input [id]="myId" type="text" value="John Doe">-->
  <!--  <input [disabled]="isDisable" type="text" value="Conan">-->`,
  styles: [`
    .text-success {
      color: green;
    }

    .text-danger {
      color: #ff7be7;
    }

    .text-special {
      font-style: italic;
    }`]

})
export class TestComponent implements OnInit {
  public name = "Doe";
  public siteUrl = window.location.href;
  public myId = "testId";
  public isDisable = true;
  public classSuccess = "text-success";
  public hasError = true;
  public isSpecial = true;
  public messageClasses ={
    "text-success": !this.hasError,
    "text-danger": this.hasError,
    "text-special": this.isSpecial
  }
  public highlightColor = "orange";
  public titleStyles = {
    color: "blue",
    fontStyle: "italic"

  }
  public greeting = "";

  constructor() {
  }

  ngOnInit() {
  }

  greetUser() {
    return "Hello " + this.name;
  }
  onClick(event){
    console.log(event);
    this.greeting = 'Welcome to Angular';
  }
  logMessage(value){
    console.log(value);
  }

}

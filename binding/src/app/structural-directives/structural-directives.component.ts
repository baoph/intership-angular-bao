import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-structural-directives',
  template: `
    <h2 *ngIf="displayName; then thenBlock;else elseBlock">John Doe</h2>

    <ng-template #thenBlock>
      <h2>
        Name is continue
      </h2>
    </ng-template>

    <ng-template #elseBlock>
      <h2>
        Name is hidden
      </h2>
    </ng-template>
    <div [ngSwitch]="color">
      <div *ngSwitchCase="'red'">You pick Red color</div>
      <div *ngSwitchCase="'blue'">You pick Green color</div>
      <div *ngSwitchCase="'green'">You pick Green color</div>
      <div *ngSwitchDefault>Pick again!</div>
    </div>
    <div *ngFor="let item of colors; index as i; first as f; last as l">
      <!--odd and even-->
      <h2> {{f}} {{i}} {{item}} {{l}}</h2>
    </div>

  `,
  styles: [`
    h2 {
      color: blueviolet;
    }`
  ]

})
export class StructuralDirectivesComponent implements OnInit {
  displayName = true;
  public color = "red";
  public colors = ['red', 'blue', 'green', 'yellow']

  constructor() {
  }

  ngOnInit() {
  }

}

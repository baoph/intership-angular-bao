import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-component-interaction',
  template: `
    <h2>hello {{name}}</h2>
    <button (click)="fireEvent()">
      Send Event
    </button>
  `,
  styles: [`
  
  `]
})
export class ComponentInteractionComponent implements OnInit {
  @Input('parentData') public name;
  @Output() public childEvent = new EventEmitter();
  fireEvent(){
    this.childEvent.emit("Hey John Doe");
  }
  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example',
  template: `
    <input [(ngModel)]="name" type="text">
    {{name}}
  `,
  styles:[]

})
export class ExampleComponent implements OnInit {
  public name="";
  constructor() { }

  ngOnInit() {
  }

}

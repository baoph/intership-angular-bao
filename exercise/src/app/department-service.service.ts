import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {throwError as observableThrowError, Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {IDepartment} from "./models/department";

@Injectable({
  providedIn: 'root'
})
export class DepartmentServiceService {

  private _url: string = "/assets/departments.json";

  constructor(private http: HttpClient) {
  }

  getDepartments(): Observable<IDepartment[]> {
    return this.http.get<IDepartment[]>(this._url).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || "Server Error");
  }
}

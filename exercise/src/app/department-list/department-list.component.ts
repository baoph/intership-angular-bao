import { Component, OnInit } from '@angular/core';
import {DepartmentServiceService} from "../department-service.service";

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  public departments = [];
  public errorMsg;

  constructor(private _departmentService: DepartmentServiceService) {
  }

  ngOnInit() {
    this._departmentService.getDepartments()
      .subscribe(data => this.departments = data,
        error => this.errorMsg = error);
  }

}

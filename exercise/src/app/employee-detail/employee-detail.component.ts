import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  public employeeId;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    //let id = parseInt(this.route.snapshot.paramMap.get('id'));
    //this.employeeId = id;
    //subscribe to paramMap to get the id from url
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.employeeId = id;
    });
  }

  goPrevious(){
    let previousId = this.employeeId - 1;
    //similar to window.location.href
    this.router.navigate(['/employees', previousId])
  }

  goNext(){
    let nextId = this.employeeId + 1;
    this.router.navigate(['/employees', nextId])
  }
  gotoEmployee(){
    let selectedId = this.employeeId ? this.employeeId : null;
    //this.router.navigate(['employees', {id: selectedId}]);
    this.router.navigate(['../', {id: selectedId}], {relativeTo: this.route});
  }
  showOverview(){
    this.router.navigate(['overview'], {relativeTo: this.route});
  }
  showContact(){
    this.router.navigate(['contact'], {relativeTo: this.route});
  }



}

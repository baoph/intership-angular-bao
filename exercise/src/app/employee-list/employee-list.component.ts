import { Component, OnInit } from '@angular/core';
import {EmployeeServiceService} from "../employee-service.service";
import {Router, ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  public employees = [];
  public errorMsg;

  constructor(private router: Router,
              private _employeeService: EmployeeServiceService,
              private route: ActivatedRoute) {
  }
public selectedId;
  ngOnInit() {
    this._employeeService.getEmployees()
      .subscribe(data => this.employees = data,
        error => this.errorMsg = error);
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.selectedId = id;
    });
  }

  onSelect(employee){
    //this.router.navigate(['/employee', employee.id]);
    this.router.navigate([employee.id], {relativeTo: this.route});
  }
  isSelected(employee){
    return employee.id === this.selectedId;
  }

}
